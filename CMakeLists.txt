project("ocv3")

cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STANDARD 17)

add_definitions(-DOCV_OPENGL)

# OCV
set(SOURCE
  "src/ocv.cpp"
  
  "src/shapes/CShape.cpp"
  
  "src/shapes/dimensions/d2/CShapeCircle2.cpp"
  "src/shapes/dimensions/d2/CShapeLine2.cpp"
  "src/shapes/dimensions/d2/CShapeLineStrip2.cpp"
  "src/shapes/dimensions/d2/CShapePolygon2.cpp"
  "src/shapes/dimensions/d2/CShapeRectangle2.cpp"
  
  "src/renderer/CRenderBuffer.cpp"
  "src/renderer/CTripleRenderBuffer.cpp"
  
  "src/shapes/properties/CShapeProperty.cpp"
  "src/shapes/properties/CShapePropertyLineWidth.cpp"
  "src/shapes/properties/CShapePropertyOutline.cpp"
  "src/shapes/properties/CShapePropertyTexture.cpp"
  "src/shapes/properties/CShapePropertyVertexColor.cpp"
)

# presets/OpenGL
set(SOURCE ${SOURCE}
  "src/presets/opengl/renderer/CRenderBuffer2.cpp"
  "src/presets/opengl/renderer/CRenderContext.cpp"

  "src/presets/opengl/shapes/dimensions/d2/CShapeCircle2.cpp"
  "src/presets/opengl/shapes/dimensions/d2/CShapeLine2.cpp"
  "src/presets/opengl/shapes/dimensions/d2/CShapeLineStrip2.cpp"
  "src/presets/opengl/shapes/dimensions/d2/CShapePolygon2.cpp"
  "src/presets/opengl/shapes/dimensions/d2/CShapeRectangle2.cpp"

  "src/presets/opengl/shapes/properties/CShapePropertyTexture.cpp"
)

add_library(${PROJECT_NAME} STATIC ${SOURCE})

target_include_directories(${PROJECT_NAME} PUBLIC "./src/")