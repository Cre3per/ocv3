#pragma once

namespace lukas::ocv
{
  class CRenderContext
  {
  public:
    /**
     * Destructor
     */
    virtual ~CRenderContext(void){};
  };
}