#pragma once

#include "CRenderBuffer.hpp"
#include "CRenderContext.hpp"

#include <memory>
#include <mutex>

namespace lukas::ocv
{
  class CShape;

  class CTripleRenderBuffer
  {
  private:

    std::mutex m_mutBuffer{ };

    /**
     * The buffer that is currently being rendered by the render thread
     */
    std::unique_ptr<CRenderBuffer> m_pRenderBuffer{ nullptr };

    /**
     * The buffer that is currently being written to by the engine thread
     */
    std::unique_ptr<CRenderBuffer> m_pWriteBuffer{ nullptr };

    /**
     * The last buffer that has been finished writing to by the engine thread.
     */
    std::unique_ptr<CRenderBuffer> m_pPendingBuffer{ nullptr };

    /**
     * The render context
     */
    std::shared_ptr<CRenderContext> m_pRenderContext{ nullptr };
    
    /**
     * Whether or not the pending buffer is ready to be rendered
     */
    bool m_bPendingBufferReady{ false };


  protected:
  public:

    /**
     * [ENGINE]
     * Adds a shape to be rendered
     * @param pShape The shape to be added
     */
    /**
     * Templated for convenience
     */
    template <class T, class = std::enable_if_t<std::is_base_of<CShape, T>::value>>
    T *addShape(T *pShape)
    {
      return dynamic_cast<T *>(this->m_pWriteBuffer->addShape(pShape));
    }

    /**
     * [RENDER]
     * Renders the most recently finished buffer.
     */
    void render(void);

    /**
     * [ENGINE]
     * Marks the current buffer as completely drawn.
     */
    void endFrame(void);

    /**
     * Default constructor
     * TODO: Delete or private
     */
    CTripleRenderBuffer(void);
    
    /**
     * Constructor
     * @param pRenderContext The render context
     */
    CTripleRenderBuffer(std::shared_ptr<CRenderContext> pRenderContext);

    /**
     * Destructor
     */
    ~CTripleRenderBuffer(void);
  };
}