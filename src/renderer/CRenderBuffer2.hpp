#pragma once

#include "renderer/CTripleRenderBuffer.hpp"

#include <assert.h>
#include <tuple>
#include <vector>


namespace lukas::ocv
{
  class CShapeCircle2;
  class CShapeLine2;
  class CShapeLineStrip2;
  class CShapePolygon2;
  class CShapeRectangle2;

  class CRenderBuffer2 : public CTripleRenderBuffer
  {
  public:
    using CTripleRenderBuffer::CTripleRenderBuffer;

    virtual CShapeCircle2 *circle(double dbX, double dbY, double dbRadius) { assert(false && "Not implemented"); return nullptr; }
    virtual CShapeLine2 *line(double db0X, double db0Y, double db1X, double db1Y) { assert(false && "Not implemented"); return nullptr; }
    virtual CShapeLineStrip2 *lineStrip(const std::vector<std::tuple<double, double>> &vVertexes) { assert(false && "Not implemented"); return nullptr; }
    virtual CShapePolygon2 *polygon(const std::vector<std::tuple<double, double>> &vVertexes) { assert(false && "Not implemented"); return nullptr; }
    virtual CShapeRectangle2 *rectangle(double db0X, double db0Y, double db3X, double db3Y) { assert(false && "Not implemented"); return nullptr; }
  };
}