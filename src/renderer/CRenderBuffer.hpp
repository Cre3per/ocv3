#pragma once

#include <vector>

namespace lukas::ocv
{
  class CRenderContext;
  class CShape;

  class CRenderBuffer
  {
  private:

    std::vector<CShape *> m_vpShapes{};

  public:

    /**
     * Adds a shape
     * @param pShape The shape to be added
     */
    CShape *addShape(CShape *pShape);

    /**
     * Renders all shapes in this buffer
     */
    void render(CRenderContext *pContext) const;

    /**
     * Frees memory and clears the buffer
     */
    void clear(void);

    /**
     * @return True if this buffer is empty
     */
    bool empty(void);
  };
}