#include "CTripleRenderBuffer.hpp"

namespace lukas::ocv
{
  /**
   * [RENDER]
   */
  void CTripleRenderBuffer::render(void)
  {
    std::unique_lock<std::mutex> lock{ this->m_mutBuffer };

    if (!this->m_bPendingBufferReady)
    {
      // The engine has not yet finished a new frame. Re-render the last frame
    }
    else
    {
      // A new frame is ready
      std::swap(this->m_pRenderBuffer, this->m_pPendingBuffer);

      this->m_pPendingBuffer->clear();
      this->m_bPendingBufferReady = false;
    }

    lock.unlock();

    // The render buffer will not be touched by the engine thread, we can render
    // it without locking the buffers.
    this->m_pRenderBuffer->render(this->m_pRenderContext.get());
  }

  /**
   * [ENGINE]
   */
  void CTripleRenderBuffer::endFrame(void)
  {
    std::unique_lock<std::mutex> lock{ this->m_mutBuffer };

    std::swap(this->m_pWriteBuffer, this->m_pPendingBuffer);
    this->m_bPendingBufferReady = true;

    lock.unlock();

    // The render thread doesn't touch the write buffer, no lock required.
    this->m_pWriteBuffer->clear();
  }

  /**
   * 
   */
  CTripleRenderBuffer::CTripleRenderBuffer(void)
  {
    this->m_pRenderBuffer.reset(new CRenderBuffer());
    this->m_pWriteBuffer.reset(new CRenderBuffer());
    this->m_pPendingBuffer.reset(new CRenderBuffer());
  }

  /**
   * 
   */
  CTripleRenderBuffer::CTripleRenderBuffer(std::shared_ptr<CRenderContext> pRenderContext)
  : CTripleRenderBuffer()
  {
    this->m_pRenderContext = pRenderContext;
  }

  /**
   * 
   */
  CTripleRenderBuffer::~CTripleRenderBuffer(void)
  {
  }
}
