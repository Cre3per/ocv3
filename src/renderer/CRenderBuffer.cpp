#include "CRenderBuffer.hpp"

#include "shapes/CShape.hpp"

namespace lukas::ocv
{
  /**
   * 
   */
  CShape *CRenderBuffer::addShape(CShape *pShape)
  {
    this->m_vpShapes.push_back(pShape);
    
    return pShape;
  }

  /**
   * 
   */
  void CRenderBuffer::render(CRenderContext *pContext) const
  {
    for (CShape *pShape : this->m_vpShapes)
    {
      pShape->render(pContext);
    }
  }

  /**
   *
   */
  void CRenderBuffer::clear(void)
  {
    for (CShape *pShape : this->m_vpShapes)
    {
      pShape->clear();
      
      delete pShape;
    }

    this->m_vpShapes.clear();
  }

  /**
   * 
   */
  bool CRenderBuffer::empty(void)
  {
    return this->m_vpShapes.empty();
  }
}