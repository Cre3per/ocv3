#include "CShape.hpp"

#include "properties/CShapeProperty.hpp"

#include "properties/CShapePropertyLineWidth.hpp"
#include "properties/CShapePropertyOutline.hpp"
#include "properties/CShapePropertyTexture.hpp"
#include "properties/CShapePropertyVertexColor.hpp"

namespace lukas::ocv
{

  /**
   * 
   */
  CShape::CShape(void)
  {
  }

  /**
   * 
   */
  CShape *CShape::addProperty(CShapeProperty *pProperty)
  {
    this->m_vProperties.push_back(pProperty);

    return this;
  }

  /**
   *
   */
  const CShapePropertyLineWidth *CShape::getLineWidth(void) const
  {
    return this->getProperty<ocv::CShapePropertyLineWidth>();
  }

  /**
   * 
   */
  const CShapePropertyOutline *CShape::getOutline(void) const
  {
    return this->getProperty<CShapePropertyOutline>();
  }

  /**
   * 
   */
  const CShapePropertyTexture *CShape::getTexture(void) const
  {
    return this->getProperty<CShapePropertyTexture>();
  }

  /**
   * 
   */
  const CShapePropertyVertexColor *CShape::getVertexColor(int iVertex) const
  {
    for (CShapeProperty *pProperty : this->m_vProperties)
    {
      CShapePropertyVertexColor * pVertexColor{ dynamic_cast<CShapePropertyVertexColor *>(pProperty) };

      if (pVertexColor != nullptr)
      {
        if (pVertexColor->getVertexId() == iVertex)
        {
          return pVertexColor;
        }
      }
    }

    return nullptr;
  }

  /**
   * 
   */
  void CShape::clear(void)
  {
    for (CShapeProperty *pProperty : this->getProperties())
    {
      delete pProperty;
    }
  }

  /**
   * 
   */
  CShape::~CShape(void)
  {
  }
}