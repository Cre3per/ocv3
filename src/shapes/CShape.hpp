#pragma once

#include <vector>


namespace lukas::ocv
{
  class CShapeProperty;
  
  class CRenderContext;

  class CShapePropertyLineWidth;
  class CShapePropertyOutline;
  class CShapePropertyTexture;
  class CShapePropertyVertexColor;

  enum class ShapePropertyType;

  class CShape
  {
  private:
  protected:
    std::vector<CShapeProperty *> m_vProperties{};

  protected:
    /**
     * @return All properties of this shape
     */
    inline std::vector<CShapeProperty *> getProperties(void)
    {
      return this->m_vProperties;
    }

    /**
     * @param type The type of the properties to return
     * @return All properties of type @type
     */
    template <class T>
    std::vector<const T *> getProperties(void) const
    {
      std::vector<T *> vpResult{};

      for (CShapeProperty *pProperty : const_cast<CShape *>(this)->getProperties())
      {
        T* pT{ dynamic_cast<T *>(pProperty) };
        if (pT != nullptr)
        {
          vpResult.push_back(pProperty);
        }
      }

      return vpResult;
    }

    /**
     * @param type The type of the property to return
     * @return The first property of type @type
     */
    template <class T>
    const T *getProperty(void) const
    {
      for (CShapeProperty *pProperty : const_cast<CShape *>(this)->getProperties())
      {
        T *pT{ dynamic_cast<T *>(pProperty) };

        if (pT != nullptr)
        {
          return pT;
        }
      }

      return nullptr;
    }

    /**
     * @return The line width as set by a property or 1.0
     */
    const CShapePropertyLineWidth *getLineWidth(void) const;

    /**
     * @return The outline property or a nullptr
     */
    const CShapePropertyOutline *getOutline(void) const;

    /**
     * @return The texture property or a nullptr
     */
    const CShapePropertyTexture *getTexture(void) const;

    /**
     * @param iVertex The index of the vertex to get the color of
     * @return The color property or a nullptr
     */
    const CShapePropertyVertexColor *getVertexColor(int iVertex) const;

    /**
     * Default constructor
     */
    CShape(void);

  public:

    /**
     * Adds a property
     * @param pProperty The property to be added
     */
    CShape *addProperty(CShapeProperty *pProperty);

    /**
     * Frees memory by deleting properties
     */
    void clear(void);

    /**
     * Renders the shape
     * @param pContext The renderer context
     */
    virtual void render(CRenderContext *pContext) const = 0;

    /**
     * Destructor
     */
    virtual ~CShape(void);
  };
}