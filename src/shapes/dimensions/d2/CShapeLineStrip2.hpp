#pragma once

#include "shapes/CShape.hpp"

#include <tuple>
#include <vector>


namespace lukas::ocv
{
  class CShapeLineStrip2 : public CShape
  {
    using vertex = std::tuple<double, double>;

  private:

  protected:

    /**
     * Vertex coordinates
     */
    std::vector<vertex> m_vVertexes{ };

  public:

    /**
     * Default constructor
     */
    CShapeLineStrip2(void);

    /**
     * Constructor
     * @param vVertexes Vertex coordinates
     */
    CShapeLineStrip2(const std::vector<vertex> &vVertexes);

  public:

    /**
     * Destructor
     */
    virtual ~CShapeLineStrip2(void);
  };
}