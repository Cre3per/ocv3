#pragma once

#include "shapes/CShape.hpp"

#include <tuple>
#include <vector>


namespace lukas::ocv
{  
  class CShapePolygon2 : public CShape
  {
    using vertex = std::tuple<double, double>;

  private:

  protected:

    /**
     * Vertex coordinates
     */
    std::vector<vertex> m_vVertexes{ };

  public:

    /**
     * Default constructor
     */
    CShapePolygon2(void);

    /**
     * Constructor
     * @param vVertexes Vertex coordinates
     */
    CShapePolygon2(const std::vector<vertex> &vVertexes);

  public:

    /**
     * Destructor
     */
    virtual ~CShapePolygon2(void);
  };
}