#pragma once

#include "shapes/CShape.hpp"

namespace lukas::ocv
{
  class CShapeRectangle2 : public CShape
  {
    /*

    Vertexes:

      0------3
      | \    |
      |   \  |
      |     \|
      1------2

    */

  private:

  protected:

    /**
     * Top left y-coordinate (Vertex 0)
     */
    double m_db0X{ 0.0 };
    
    /**
     * Top left y-coordinate (Vertex 0)
     */
    double m_db0Y{ 0.0 };

    /**
     * Bottom right x-coordinate (Vertex 3)
     */
    double m_db3X{ 0.0 };

    /**
     * Bottom right y-coordinate (Vertex 3)
     */
    double m_db3Y{ 0.0 };

  public:

    /**
     * Default constructor
     */
    CShapeRectangle2(void);

    /**
     * Constructor
     * @param db0X Top left y-coordinate (Vertex 0)
     * @param db0Y Top left x-coordinate (Vertex 0)
     * @param db3X Bottom right x-coordinate (Vertex 3)
     * @param db3Y Bottom right y-coordinate (Vertex 3)
     */
    CShapeRectangle2(double db0X, double db0Y, double db3X, double db3Y);

  public:

    /**
     * Destructor
     */
    virtual ~CShapeRectangle2(void);
  };
}