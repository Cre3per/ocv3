#pragma once

#include "shapes/CShape.hpp"

namespace lukas::ocv
{
  class CShapeLine2 : public CShape
  {
  private:
  protected:
    /**
     * Start x coordinate (Vertex 0)
     */
    double m_db0X{ 0.0 };

    /**
     * Start y coordinate (Vertex 0)
     */
    double m_db0Y{ 0.0 };

    /**
     * End x coordinate (Vertex 1)
     */
    double m_db1X{ 0.0 };

    /**
     * End y coordinate (Vertex 1)
     */
    double m_db1Y{ 0.0 };

  public:

    /**
     * Default constructor
     */
    CShapeLine2(void);

    /**
     * Constructor
     * @param db0X Start x coordinate (Vertex 0)
     * @param db1Y Start y coordinate (Vertex 0)
     * @param db1X End x coordinate (Vertex 1)
     * @param db1Y End y coordinate (Vertex 1)
     */
    CShapeLine2(double db0X, double db0Y, double db1X, double db1Y);

  public:
    /**
     * Destructor
     */
    virtual ~CShapeLine2(void);
  };
}