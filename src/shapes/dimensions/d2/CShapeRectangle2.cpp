#include "CShapeRectangle2.hpp"

namespace lukas::ocv
{
  /**
   * 
   */
  CShapeRectangle2::CShapeRectangle2(void)
  {
    
  }

  /**
   *
   */
  CShapeRectangle2::CShapeRectangle2(double db0X, double db0Y, double db3X, double db3Y)
  : m_db0X(db0X),
    m_db0Y(db0Y),
    m_db3X(db3X),
    m_db3Y(db3Y)
  {

  }

  /**
   * 
   */
  CShapeRectangle2::~CShapeRectangle2(void)
  {
    
  }
}