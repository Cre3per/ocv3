#include "CShapeCircle2.hpp"

namespace lukas::ocv
{
  /**
   * 
   */
  CShapeCircle2::CShapeCircle2(void)
  {
  }

  /**
   * 
   */
  CShapeCircle2::CShapeCircle2(double dbX, double dbY, double dbRadius)
  : m_dbX(dbX),
    m_dbY(dbY),
    m_dbRadius(dbRadius)
  {
    
  }


  /**
   * 
   */
  CShapeCircle2::~CShapeCircle2(void)
  {
  }

}