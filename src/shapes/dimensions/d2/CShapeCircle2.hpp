#pragma once

#include "shapes/CShape.hpp"

#include <tuple>
#include <vector>


namespace lukas::ocv
{
  class CShapeCircle2 : public CShape
  {
  private:

  protected:

    /**
     * Center x-coordinate
     */
    double m_dbX{ 0.0 };

    /**
     * Center y-coordinate
     */
    double m_dbY{ 0.0 };

    /**
     * Radius
     */
    double m_dbRadius{ 0.0 };

  public:

    /**
     * Default constructor
     */
    CShapeCircle2(void);

    /**
     * Constructor
     * @param dbX Center x-Coordinate
     * @param dbY Center y-Coordinate
     * @param dbRadius Radius
     */
    CShapeCircle2(double dbX, double dbY, double dbRadius);

  public:

    /**
     * Destructor
     */
    virtual ~CShapeCircle2(void);
  };
}