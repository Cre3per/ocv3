#include "CShapeLine2.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"


namespace lukas::ocv
{

  /**
   * 
   */
  CShapeLine2::CShapeLine2(void)
  {
    
  }

  /**
   *
   */
  CShapeLine2::CShapeLine2(double db0X, double db0Y, double db1X, double db1Y)
  : m_db0X(db0X),
    m_db0Y(db0Y),
    m_db1X(db1X),
    m_db1Y(db1Y)
  {

  }

  /**
   * 
   */
  CShapeLine2::~CShapeLine2(void)
  {
    
  }
}