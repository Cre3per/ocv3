#include "CShapeProperty.hpp"

namespace lukas::ocv
{
  class CShapePropertyOutline : public CShapeProperty
  {
  private:
  public:
    /**
   * Default constructor
   */
    CShapePropertyOutline(void);

    /**
   * Destructor
   */
    virtual ~CShapePropertyOutline(void);
  };
}