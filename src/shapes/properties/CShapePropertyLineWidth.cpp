#include "CShapePropertyLineWidth.hpp"

namespace lukas::ocv
{

  /**
   * 
   */
  CShapePropertyLineWidth::CShapePropertyLineWidth(double dbLineWidth)
  {
    this->m_dbValue = dbLineWidth;
  }

  /**
   * 
   */
  CShapePropertyLineWidth::CShapePropertyLineWidth(void)
  {
  }

  /**
   *
   */
  CShapePropertyLineWidth::~CShapePropertyLineWidth(void)
  {
  }
}