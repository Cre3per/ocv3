#include "CShapeProperty.hpp"

namespace lukas::ocv
{
  class CShapePropertyVertexColor : public CShapeProperty
  {
  private:
    int m_iVertexId{ 0 };

    double m_dbR{ 0.0 };
    double m_dbG{ 0.0 };
    double m_dbB{ 0.0 };
    double m_dbA{ 0.0 };

  public:

    /**
     * @return Target vertex index
     */
    inline int getVertexId(void) const
    {
      return this->m_iVertexId;
    }

    /**
     * @return Red component
     */
    inline double getR(void) const
    {
      return this->m_dbR;
    }

    /**
     * @return Green component
     */
    inline double getG(void) const
    {
      return this->m_dbG;
    }

    /**
     * @return Blue component
     */
    inline double getB(void) const
    {
      return this->m_dbB;
    }

    /**
     * @return Alpha component
     */
    inline double getA(void) const
    {
      return this->m_dbA;
    }

    /**
     * @return Color in RGBA format
     */
    unsigned int getRGBA(void) const;

    /**
     * Constructor
     * @param iVertexId The id of the vertex to color
     * @param r Red
     * @param g Green
     * @param b Blue
     * @param a Alpha
     */
    CShapePropertyVertexColor(int iVertexId, double r, double g, double b, double a);

    /**
     * Constructor
     * @param iVertexId The id of the vertex to color
     * @param uiRGBA Color in RGBA format
     */
    CShapePropertyVertexColor(int iVertexId, unsigned int uiRGBA);

    /**
     * Default constructor
     */
    CShapePropertyVertexColor(void);

    /**
     * Destructor
     */
    virtual ~CShapePropertyVertexColor(void);
  };
}