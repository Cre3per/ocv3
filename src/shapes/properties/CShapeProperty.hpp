#pragma once

#include <string>

namespace lukas::ocv
{
  class CShapeProperty
  {
  private:
  protected:

    /**
     * Default constructor
     */
    CShapeProperty(void);

  public:

    /**
     * Destructor
     */
    virtual ~CShapeProperty(void);
  };
}