#pragma once

#include "CShapeProperty.hpp"

namespace lukas::ocv
{
  class CShapePropertyTexture : public CShapeProperty
  {
  private:
  
  public:

    /**
     * Default constructor
     */
    CShapePropertyTexture(void);

    /**
     * Destructor
     */
    virtual ~CShapePropertyTexture(void);
  };
}