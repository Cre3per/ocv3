#include "CShapePropertyVertexColor.hpp"

namespace lukas::ocv
{

  /**
   * 
   */
  unsigned int CShapePropertyVertexColor::getRGBA(void) const
  {
    unsigned int uiResult{ 0x00000000 };

    uiResult |= static_cast<unsigned int>(255.0 * this->getR()) << 24;
    uiResult |= static_cast<unsigned int>(255.0 * this->getG()) << 16;
    uiResult |= static_cast<unsigned int>(255.0 * this->getB()) << 8;
    uiResult |= static_cast<unsigned int>(255.0 * this->getA());

    return uiResult;
  }

  /**
   * 
   */
  CShapePropertyVertexColor::CShapePropertyVertexColor(int iVertexId, double r, double g, double b, double a)
      : m_iVertexId(iVertexId),
        m_dbR(r), m_dbG(g), m_dbB(b), m_dbA(a)
  {
  }

  /**
   * 
   */
  CShapePropertyVertexColor::CShapePropertyVertexColor(int iVertexId, unsigned int uiRGBA)
    : m_iVertexId(iVertexId),
      m_dbR(static_cast<double>((uiRGBA & 0xFF000000) >> 24) / 255.0),
      m_dbG(static_cast<double>((uiRGBA & 0x00FF0000) >> 16) / 255.0),
      m_dbB(static_cast<double>((uiRGBA & 0x0000FF00) >> 8) / 255.0),
      m_dbA(static_cast<double>(uiRGBA & 0x000000FF) / 255.0)
  {
  }

  /**
   * 
   */
  CShapePropertyVertexColor::CShapePropertyVertexColor(void)
  {
  }

  /**
   * 
   */
  CShapePropertyVertexColor::~CShapePropertyVertexColor(void)
  {
  }
}