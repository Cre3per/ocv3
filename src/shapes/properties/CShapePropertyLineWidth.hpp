#pragma once

#include "CShapeProperty.hpp"

namespace lukas::ocv
{
  class CShapePropertyLineWidth : public CShapeProperty
  {
  private:
    double m_dbValue{ 1.0 };

  public:

    /**
     * @return Desired line width
     */
    inline double getValue(void) const
    {
      return this->m_dbValue;
    }

    /**
     * Constructor
     * @param dbWidth Desired ine width
     */
    CShapePropertyLineWidth(double dbWidth);

    /**
     * Default constructor
     */
    CShapePropertyLineWidth(void);

    /**
     * Destructor
     */
    virtual ~CShapePropertyLineWidth(void);
  };
}