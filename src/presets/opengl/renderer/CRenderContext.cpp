#if defined(OCV_OPENGL)

#include "CRenderContext.hpp"

#include <iostream> // TODO: DBG


namespace lukas::ocv::ROpenGL
{
  void CRenderContext::getResolution(double &dbResolutionX, double &dbResolutionY) const
  {
    dbResolutionX = this->m_dbResolutionX;
    dbResolutionY = this->m_dbResolutionY;
  }

  void CRenderContext::setResolution(double dbResolutionX, double dbResolutionY)
  {
    this->m_dbResolutionX = dbResolutionX;
    this->m_dbResolutionY = dbResolutionY;
  }

  double CRenderContext::getAspectRatio(void) const
  {
    if (this->m_dbResolutionY == 0.0)
    {
      return 1.0;
    }
    else
    {
      return (this->m_dbResolutionX / this->m_dbResolutionY);
    }    
  }

  /**
   * 
   */
  CRenderContext::~CRenderContext(void)
  {
    
  }
}

#endif