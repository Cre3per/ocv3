#if defined(OCV_OPENGL)

#include "CRenderBuffer2.hpp"

#include "presets/opengl/shapes/dimensions/d2/CShapeCircle2.hpp"
#include "presets/opengl/shapes/dimensions/d2/CShapeLine2.hpp"
#include "presets/opengl/shapes/dimensions/d2/CShapeLineStrip2.hpp"
#include "presets/opengl/shapes/dimensions/d2/CShapePolygon2.hpp"
#include "presets/opengl/shapes/dimensions/d2/CShapeRectangle2.hpp"


namespace lukas::ocv::ROpenGL
{
  ocv::CShapeCircle2 *CRenderBuffer2::circle(double dbX, double dbY, double dbRadius)
  {
    return this->addShape(new CShapeCircle2(dbX, dbY, dbRadius));
  }

  ocv::CShapeLine2 *CRenderBuffer2::line(double x1, double y1, double x2, double y2)
  {
    return this->addShape(new CShapeLine2(x1, y1, x2, y2));
  }

  ocv::CShapeLineStrip2 *CRenderBuffer2::lineStrip(const std::vector<std::tuple<double, double>> &vVertexes)
  {
    return this->addShape(new CShapeLineStrip2(vVertexes));
  }

  ocv::CShapePolygon2 *CRenderBuffer2::polygon(const std::vector<std::tuple<double, double>> &vVertexes)
  {
    return this->addShape(new CShapePolygon2(vVertexes));
  }

  ocv::CShapeRectangle2 *CRenderBuffer2::rectangle(double x1, double y1, double x2, double y2)
  {
    return this->addShape(new CShapeRectangle2(x1, y1, x2, y2));
  }
}

#endif