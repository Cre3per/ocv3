#if defined(OCV_OPENGL)

#pragma once

#include "renderer/CRenderContext.hpp"

namespace lukas::ocv::ROpenGL
{
  class CRenderContext : public ocv::CRenderContext
  {
  private:
    double m_dbResolutionX{ 0.0 };
    double m_dbResolutionY{ 0.0 };

  public:

    // TODO: Doc

    void getResolution(double &dbResolutionX, double &dbResolutionY) const;

    void setResolution(double dbResolutionX, double dbResolutionY);

    double getAspectRatio(void) const;


    /**
     * Destructor
     */
    virtual ~CRenderContext(void);
  };
}

#endif