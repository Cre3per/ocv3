#if defined(OCV_OPENGL)
#pragma once

#include "renderer/CRenderBuffer2.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapeCircle2;
  class CShapeLine2;
  class CShapeLineStrip2;
  class CShapePolygon2;
  class CShapeRectangle2;

  class CRenderBuffer2 : public ocv::CRenderBuffer2
  {
  public:

    using ocv::CRenderBuffer2::CRenderBuffer2;

    virtual ocv::CShapeCircle2 *circle(double dbX, double dbY, double dbRadius) override;
    virtual ocv::CShapeLine2 *line(double x1, double y1, double x2, double y2) override;
    virtual ocv::CShapeLineStrip2 *lineStrip(const std::vector<std::tuple<double, double>> &vVertexes) override;
    virtual ocv::CShapePolygon2 *polygon(const std::vector<std::tuple<double, double>> &vVertexes);
    virtual ocv::CShapeRectangle2 *rectangle(double x1, double y1, double x2, double y2) override;
  };
}

#endif