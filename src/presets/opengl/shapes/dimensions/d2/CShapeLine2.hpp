#if defined(OCV_OPENGL)
#pragma once

#include "shapes/dimensions/d2/CShapeLine2.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapeLine2 : public ocv::CShapeLine2
  {
    /**
     * 
     */
    virtual void render(ocv::CRenderContext *pRenderContext) const override;

  public:
    using ocv::CShapeLine2::CShapeLine2;

    /**
     * Destructor
     */
    virtual ~CShapeLine2(void) {}
  };
}

#endif