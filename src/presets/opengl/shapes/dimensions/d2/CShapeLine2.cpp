#if defined(OCV_OPENGL)

#include "CShapeLine2.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"
#include "shapes/properties/CShapePropertyVertexColor.hpp"

#include <GL/gl.h>

namespace lukas::ocv::ROpenGL
{
  void CShapeLine2::render(ocv::CRenderContext *pRenderContext) const
  {
    glPushMatrix();

    double dbOldLineWidth{ 0.0 };
    const lukas::ocv::CShapePropertyLineWidth *pLineWidth{ this->getLineWidth() };

    if (pLineWidth != nullptr)
    {
      glGetDoublev(GL_LINE_WIDTH, &dbOldLineWidth);
      glLineWidth(pLineWidth->getValue());
    }

    glBegin(GL_LINES);

    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(0) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    glVertex2d(this->m_db0X, this->m_db0Y);

    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(1) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    glVertex2d(this->m_db1X, this->m_db1Y);

    glEnd();

    if (pLineWidth != nullptr)
    {
      glLineWidth(dbOldLineWidth);
    }

    glPopMatrix();
  }
}

#endif