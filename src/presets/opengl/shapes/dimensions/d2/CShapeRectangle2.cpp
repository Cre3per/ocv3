#if defined(OCV_OPENGL)

#include "CShapeRectangle2.hpp"

#include "presets/opengl/shapes/properties/CShapePropertyTexture.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"
#include "shapes/properties/CShapePropertyVertexColor.hpp"

#include <GL/gl.h>

namespace lukas::ocv::ROpenGL
{
  /**
   * 
   */
  void CShapeRectangle2::render(ocv::CRenderContext *pRenderContext) const
  {
    glPushMatrix();

    const ROpenGL::CShapePropertyTexture *pTexture{ this->getProperty<ROpenGL::CShapePropertyTexture>() };

    if (pTexture != nullptr)
    {
      glBindTexture(GL_TEXTURE_2D, pTexture->getTextureId());
    }

    if (this->getOutline())
    {
      const lukas::ocv::CShapePropertyLineWidth * pLineWidth{ this->getLineWidth() };

      glLineWidth((pLineWidth == nullptr) ? 1.0 : pLineWidth->getValue());


      glBegin(GL_LINE_LOOP);
    }
    else
    {

      glBegin(GL_TRIANGLE_FAN);
    }

    // 0
    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(0) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    if (pTexture != nullptr)
    {
      glTexCoord2d(0.0, 0.0);
    }

    glVertex2d(this->m_db0X, this->m_db0Y);

    // 1
    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(1) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    if (pTexture != nullptr)
    {
      glTexCoord2d(0.0, 1.0);
    }

    glVertex2d(this->m_db0X, this->m_db3Y);

    // 2
    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(2) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    if (pTexture != nullptr)
    {
      glTexCoord2d(1.0, 1.0);
    }

    glVertex2d(this->m_db3X, this->m_db3Y);

    // 3
    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(3) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    if (pTexture != nullptr)
    {
      glTexCoord2d(1.0, 0.0);
    }

    glVertex2d(this->m_db3X, this->m_db0Y);

    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    glPopMatrix();
  }
}

#endif
