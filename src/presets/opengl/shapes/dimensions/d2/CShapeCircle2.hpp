#if defined(OCV_OPENGL)
#pragma once

#include "shapes/dimensions/d2/CShapeCircle2.hpp"
#include "presets/opengl/renderer/CRenderContext.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapeCircle2 : public ocv::CShapeCircle2
  {
    /**
     * 
     */
    virtual void render(ocv::CRenderContext *pRenderContext) const override;

  public:
    using ocv::CShapeCircle2::CShapeCircle2;

    /**
     * Destructor
     */
    virtual ~CShapeCircle2(void) {}
  };
}

#endif