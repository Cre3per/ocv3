#if defined(OCV_OPENGL)
#pragma once

#include "shapes/dimensions/d2/CShapeLineStrip2.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapeLineStrip2 : public ocv::CShapeLineStrip2
  {
    /**
     * 
     */
    virtual void render(ocv::CRenderContext *pRenderContext) const override;

  public:
    using ocv::CShapeLineStrip2::CShapeLineStrip2;

    /**
     * Destructor
     */
    virtual ~CShapeLineStrip2(void) {}
  };
}

#endif