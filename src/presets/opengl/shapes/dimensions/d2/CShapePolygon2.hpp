#if defined(OCV_OPENGL)
#pragma once

#include "shapes/dimensions/d2/CShapePolygon2.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapePolygon2 : public ocv::CShapePolygon2
  {
    /**
     * 
     */
    virtual void render(ocv::CRenderContext *pRenderContext) const override;

  public:
    using ocv::CShapePolygon2::CShapePolygon2;

    /**
     * Destructor
     */
    virtual ~CShapePolygon2(void) {}
  };
}

#endif