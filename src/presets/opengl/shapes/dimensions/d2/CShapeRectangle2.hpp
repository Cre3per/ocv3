#if defined(OCV_OPENGL)
#pragma once

#include "shapes/dimensions/d2/CShapeRectangle2.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapeRectangle2 : public ocv::CShapeRectangle2
  {
    /**
     * 
     */
    virtual void render(ocv::CRenderContext *pRenderContext) const override;

  public:
    using ocv::CShapeRectangle2::CShapeRectangle2;

    /**
     * Destructor
     */
    virtual ~CShapeRectangle2(void){    }
  };
}

#endif