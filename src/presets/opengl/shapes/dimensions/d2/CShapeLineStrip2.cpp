#if defined(OCV_OPENGL)

#include "CShapeLineStrip2.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"
#include "shapes/properties/CShapePropertyVertexColor.hpp"

#include <GL/gl.h>

namespace lukas::ocv::ROpenGL
{
  void CShapeLineStrip2::render(ocv::CRenderContext *pRenderContext) const
  {
    glPushMatrix();

    const lukas::ocv::CShapePropertyLineWidth *pLineWidth{ this->getLineWidth() };

    glLineWidth((pLineWidth == nullptr) ? 1.0 : pLineWidth->getValue());

    glBegin(GL_LINE_STRIP);

    for (size_t s{ 0 }; s < this->m_vVertexes.size(); ++s)
    {
      if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(static_cast<int>(s)) })
      {
        glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
      }

      glVertex2d(std::get<0>(this->m_vVertexes.at(s)), std::get<1>(this->m_vVertexes.at(s)));
    }

    glEnd();

    glPopMatrix();
  }
}

#endif