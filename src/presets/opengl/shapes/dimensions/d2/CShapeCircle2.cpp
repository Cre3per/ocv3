#if defined(OCV_OPENGL)

#include "CShapeCircle2.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"
#include "shapes/properties/CShapePropertyVertexColor.hpp"

#include "presets/opengl/renderer/CRenderContext.hpp"

#include <GL/gl.h>

#include <cmath>


namespace lukas::ocv::ROpenGL
{
  void CShapeCircle2::render(ocv::CRenderContext *pRenderContext) const
  {
    const CRenderContext *pContext{ dynamic_cast<CRenderContext *>(pRenderContext) };

    double dbAspectRatio{ pContext->getAspectRatio() };

    glPushMatrix();

    const int iDetail{ 36 };

    if (this->getOutline() != nullptr)
    {
      const lukas::ocv::CShapePropertyLineWidth * pLineWidth{ this->getLineWidth() };

      glLineWidth((pLineWidth == nullptr) ? 1.0 : pLineWidth->getValue());

      glBegin(GL_LINE_LOOP);
    }
    else
    {
      glBegin(GL_TRIANGLE_FAN);
    }

    if (const lukas::ocv::CShapePropertyVertexColor * pColor{ this->getVertexColor(0) })
    {
      glColor4d(pColor->getR(), pColor->getG(), pColor->getB(), pColor->getA());
    }

    for (int i{ 0 }; i < iDetail; ++i)
    {
      double dbProgress{ (static_cast<double>(i) / iDetail) };

      dbProgress *= (M_PI * 2.0);

      double dbSin{ sin(dbProgress) };
      double dbCos{ cos(dbProgress) };

      double dbX{ this->m_dbX + this->m_dbRadius * dbSin };
      double dbY{ this->m_dbY + this->m_dbRadius * dbCos * dbAspectRatio };

      glVertex2d(dbX, dbY);
    }

    glEnd();

    glPopMatrix();
  }
}

#endif
