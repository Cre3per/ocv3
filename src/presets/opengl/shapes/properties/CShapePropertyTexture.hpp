#if defined(OCV_OPENGL)

#pragma once

#include "shapes/properties/CShapePropertyTexture.hpp"

namespace lukas::ocv::ROpenGL
{
  class CShapePropertyTexture : public ocv::CShapePropertyTexture
  {
  protected:
    unsigned int m_uiTextureId{ 0 };

  public:

    /**
     * @return The OpenGL texture id
     */
    int getTextureId(void) const;

    /**
     * Constructor
     */
    CShapePropertyTexture(unsigned int uiTextureId);
  };
}

#endif