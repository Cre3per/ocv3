#if defined(OCV_OPENGL)

#include "CShapePropertyTexture.hpp"

namespace lukas::ocv::ROpenGL
{
  int CShapePropertyTexture::getTextureId(void) const
  {
    return this->m_uiTextureId;
  }

  CShapePropertyTexture::CShapePropertyTexture(unsigned int uiTextureId)
      : m_uiTextureId(uiTextureId)
  {
  }
}

#endif