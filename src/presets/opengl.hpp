#pragma once

#include "opengl/renderer/CRenderBuffer2.hpp"
#include "opengl/renderer/CRenderContext.hpp"

#include "opengl/shapes/dimensions/d2/CShapeCircle2.hpp"
#include "opengl/shapes/dimensions/d2/CShapeLine2.hpp"
#include "opengl/shapes/dimensions/d2/CShapePolygon2.hpp"
#include "opengl/shapes/dimensions/d2/CShapeRectangle2.hpp"

#include "opengl/shapes/properties/CShapePropertyTexture.hpp"