#pragma once



#include "renderer/CRenderBuffer2.hpp"
#include "renderer/CRenderBuffer3.hpp"
#include "renderer/CRenderContext.hpp"
#include "renderer/CTripleRenderBuffer.hpp"

#include "shapes/dimensions/d2/CShapeCircle2.hpp"
#include "shapes/dimensions/d2/CShapeLine2.hpp"
#include "shapes/dimensions/d2/CShapeLineStrip2.hpp"
#include "shapes/dimensions/d2/CShapePolygon2.hpp"
#include "shapes/dimensions/d2/CShapeRectangle2.hpp"

#include "shapes/properties/CShapePropertyLineWidth.hpp"
#include "shapes/properties/CShapePropertyOutline.hpp"
#include "shapes/properties/CShapePropertyTexture.hpp"
#include "shapes/properties/CShapePropertyVertexColor.hpp"

#if defined(OCV_OPENGL)
#include "presets/opengl.hpp"
#endif


#include <memory>
#include <typeinfo>

namespace lukas::ocv
{

  template <class TRenderBuffer2, class TRenderBuffer3, class TRenderContext>
  class COCV;

  #if defined(OCV_OPENGL)
  using COCVT = lukas::ocv::COCV<
    ROpenGL::CRenderBuffer2,
    CRenderBuffer3, // TODO:
    ROpenGL::CRenderContext>;
  #endif

  template <class TRenderBuffer2, class TRenderBuffer3, class TRenderContext>
  class COCV
  {
  private:

    std::unique_ptr<TRenderBuffer2> m_pD2{ nullptr };
    std::unique_ptr<TRenderBuffer3> m_pD3{ nullptr };

    std::shared_ptr<TRenderContext> m_pRenderContext{ nullptr };

  public:

    /**
     * @return 2D renderer
     */
    TRenderBuffer2 *d2(void)
    {
      return this->m_pD2.get();
    }

    /**
     * @return 3D renderer
     */
    TRenderBuffer3 *d3(void)
    {
      return this->m_pD3.get();
    }

    /**
     * @return Renderer context
     */
    TRenderContext *context(void)
    {
      return this->m_pRenderContext.get();
    }


    /**
     * Default constructor
     */
    COCV(void)
    {
      this->m_pRenderContext.reset(new TRenderContext());
      
      this->m_pD2.reset(new TRenderBuffer2(this->m_pRenderContext));
      this->m_pD3.reset(new TRenderBuffer3());
    }

  };
}